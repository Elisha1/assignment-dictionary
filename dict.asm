global find_word
extern string_equals
extern string_length

section .text
find_word:
	xor rax, rax
.loop:
		test rsi, rsi
		jz .end
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		test rax, rax
		jnz .found
		mov rsi, [rsi]
		jmp .loop
.found:
	mov r8, 8
	mov rdi, rsi
	add rdi, r8
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	add rdi, 1
	ret

.end:
	xor rdi, rdi
	ret
