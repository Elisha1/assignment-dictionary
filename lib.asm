section .text

GLOBAL exit
GLOBAL string_length
GLOBAL print_string
GLOBAL print_char
GLOBAL print_newline
GLOBAL print_uint
GLOBAL print_int
GLOBAL string_equals
GLOBAL read_char
GLOBAL read_word
GLOBAL parse_uint
GLOBAL parse_int
GLOBAL string_copy

;accepts return code and terminates the cureent process
exit:
	mov rax, 60  
	xor rdi, rdi
	syscall

;Takes a pointer to a null-terminated string, returns its length
string_length:
	 mov    rax, 0
	 .loop:
	     xor rax, rax
	 .count:
	     cmp byte [rdi+rax], 0
	     je .end
	     inc rax
	     jmp .count
	 .end:
	     ret

; Takes a pointer to a null-terminated string, returns it in stdout
print_string:
	call string_length
	mov   rdx, rax   ;the length of the string	
	mov   rsi, rdi  ;string address
	xor rdi, rdi ;stdout descriptor
	mov   rax,  1 	
	mov   rdi, 1
      	syscall
      	ret

;accepts code symbol and output in stdout
print_char:
	mov rsi, rsp
	dec rsi
	mov byte[rsi], dil
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	ret
	
print_newline:
	mov r8, 10
    	mov rdi, r8
	jmp print_char

;Outputs an unsigned 8-byte number in decimal format
;Allocate space on the stack and store the division results there
;Don't forget to convert the numbers to their ASCII codes
print_uint:
	mov rax, rdi  	;divisor in accumulator
	mov r8, 10	;dividend in some register
	mov r9, rsp
	dec rsp
.division:
	mov rdx, 0
	div r8
	add rdx, 0x30
.forStack:
	dec rsp
	mov byte[rsp], dl
	cmp rax, 0
	jnz .division
.print:
	mov rdi, rsp
	call print_string
	mov rsp, r9
	ret

;Displays a signed 8-byte number in decimal format
print_int:
	mov rdx, 0
	cmp rdi, rdx
	js .negative 	; for positive numbers
	jmp .positive
.negative:	
	push rdi
	mov rdi, '-'	;writing the negative sign
	call print_char
	pop rdi
	neg rdi
.positive:
	call print_uint ;printing the positive number
	ret
	

  
;Takes 2 pointers to null-terminated strings, returns 1 if they are equal, 0 otherwise
string_equals:
	mov rax, 0
	mov al, [rdi]
	cmp al, [rsi]
	jnz .finish
	inc rsi
	add rdi, 1
	test al, al
	jnz string_equals
	mov rax, 1
	ret
.finish:
	mov rax, 0
	ret

;Reads a character from stdin and returns it. Returns 0 if the end of the stream has been reached
read_char:
	push 0
	mov rsi, rsp
	mov rdx, 1
	xor rdi, rdi
	xor rax, rax
	syscall
	pop rax
 	ret

;Accepts the address of the beginning of the buffer, the size of the buffer
;Reads a word from stdin into the buffer, skipping leading whitespace
;Whitespace characters => 0x2, tab => 0x9 tab and change line => 0xA
;Stops and returns 0 if the word is too large for the buffer
;On success, returns the address of the buffer in rax, the length off the word in rdx 
;Returns  on failure in rax
;This function must append a null terminator to the word
read_word:
	mov r8, rdi
	mov r9, rsi
	mov r10, rdi
.repeat:
	call read_char
	cmp rax, 0xA
	je .repeat
	cmp rax, 0x9
	je .repeat
	cmp rax, 0x20
	je .repeat
.read:
	cmp rax, 0x0
	je .finish
	cmp rax, 0xA
	je .finish
	cmp rax, 0x9
	je .finish
	cmp rax, 0x20
	je .finish
	dec r9
	cmp r9, 0
	je .stop
	mov byte[r8], al
	inc r8
	call read_char
	jmp .read
.stop:
	xor rax, rax
	ret
.finish:
	mov byte[r8], 0
	mov rdx, r8
	sub rdx, r10
	mov rax, r10
	ret

; Takes pointer to a string and tries to read an unsigned number from its beginning.
; returns rax: number, rdx : length of it's characters
;rdx=0 if the number could not be read
parse_uint:
	mov rdx, 0
	mov rcx, rax
	mov rax, 0
	mov rsi, rdi
	mov r8, 10
	
.repeat:
	mov rdi, 0
	mov dil, byte[rsi+rdx]
	cmp dil, '0'
	jb .finish
	cmp dil, '9'
	ja .finish
	sub dil, '0'
	imul rax, r8
	add rax, rdi
	inc rdx
	dec rcx
	jne .repeat
.finish:
	ret

;Takes a pointer to a string, tries to read a signed number from the beginning.
;If there is a character, spaces between it and the number are not allowed
;returns rax: number, rdx : length of it's characters
;rdx=0 if the number could not be read
parse_int:
	mov r8, 0
.work:
	mov al, byte[rdi]
	cmp al, "-"
	jne parse_uint
	inc rdi
	call parse_uint
	cmp rdx, r8
	je .finish
	inc rdx
	neg rax
.finish:
	ret 

;Accepts a pointer to a string, a pointer to a buffer, and the length of the buffer
;Copies a line into the clipboard
;Returns the length of the string if it fits into the buffer, otherwise 0
string_copy:
.loop:
	call string_length   ; calls string length to rax
	inc rax
	cmp rdx, 0
	je .finish
	cmp rax, rdx
	jg .onMinus
	mov al, [rdi]
	mov [rsi], al
	cmp byte[rdi], 0
	je .copy
	inc rsi
	inc rdi
	dec rdx
	jmp .loop
.copy:
	mov rax, rsi

.onMinus: 
	mov rax, 0 
	ret
	
.finish:
	xor rax, rax
	ret
	
	
	
	
	
